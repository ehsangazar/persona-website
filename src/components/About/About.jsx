import React from "react";
import Layout from "../Layout/Layout";
import './About.css'
import Button from "../Button/Button";
import Space from "../Space/Space";
import introSrc from './intro.mp4'

const About = () => {
  return <Layout>
    <div className="About">
      <div className="About--VideoSection">
        <video src={introSrc} />
        <div className="About--PrimaryBox" />
      </div>
      <div className="About--ContentSection">
        <h1>
          Senior Software Engineer
        </h1>
        <h3>
          Developer for more than a decade
        </h3>
        <p>
          Irure aliqua laborum duis aliqua aute qui Lorem nisi ipsum qui nostrud laborum sunt. Laborum reprehenderit eu non elit deserunt minim laborum reprehenderit adipisicing est elit. Commodo fugiat do laboris esse ad aliquip non nulla cillum irure do nulla. Cillum magna tempor sunt dolor reprehenderit minim.
        </p>
        <Space />
        <Space />
        <Button>Download Resume</Button>
      </div>
    </div>
  </Layout>;
};

export default About;
