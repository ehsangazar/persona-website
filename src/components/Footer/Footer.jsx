import React from "react";
import Icons from "../Icons/Icons";
import './Footer.css'

const Footer = () => {
  return (
    <footer className="Footer">
      <ul>
        <li>
          <a href="https://www.facebook.com/ehsangazarcom">
            <Icons name="FaceBook" />
          </a>
        </li>
        <li>
          <a href="https://twitter.com/ehsangazar">
            <Icons name="Twitter" />
          </a>
        </li>
      </ul>
      <span className="copyright">© 2020. All rights reserved.</span>
    </footer>
  );
};

export default Footer;
