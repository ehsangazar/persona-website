import React from "react";
import Button from "../Button/Button";
import Layout from "../Layout/Layout";
import Space from "../Space/Space";
import "./Portfolio.css";

const Portfolio = () => {
  return (
    <Layout>
      <div className="Portfolio">
        <div className="Portfolio--ContentSection">
          <h1>Let you see my Portfolio</h1>
          <Space />
          <div>
            <p>
              Irure aliqua laborum duis aliqua aute qui Lorem nisi ipsum qui
              nostrud laborum sunt. Laborum reprehenderit eu non elit deserunt
              minim laborum reprehenderit adipisicing est elit. Commodo fugiat
              do laboris esse ad aliquip non nulla cillum irure do nulla. Cillum
              magna tempor sunt dolor reprehenderit minim.
            </p>
          </div>
          <Space />
          <div>
            <Button>SEE MY PORTFOLIO</Button>
          </div>
        </div>
        <div className="Portfolio--BoxSection">
          <div className="Portfolio--BoxBox">
            <div className="Portfolio--FirstBox"></div>
            <div className="Portfolio--SecondBox"></div>
          </div>
          <div className="Portfolio--BoxBox2">
            <div className="Portfolio--ThirdBox"></div>
            <div className="Portfolio--FourthBox"></div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Portfolio;
